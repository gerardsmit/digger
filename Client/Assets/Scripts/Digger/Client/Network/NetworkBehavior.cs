﻿using Digger.Client.Behavior.Map;
using Digger.Common;
using LiteNetLib;
using UnityEngine;

namespace Digger.Client.Network
{
	public class NetworkBehavior : MonoBehaviour {
		private readonly NetManager _manager;
		
		public WorldController WorldController => GameObject.FindGameObjectWithTag("World").GetComponent<WorldController>();

		public NetworkBehavior()
		{
			_manager = new NetManager(new NetEventListener(this), "Digger");
		}

		private void Awake()
		{
			Environment.Side = Side.Client;
		}

		private void OnEnable()
		{
			_manager.Start();
			_manager.Connect("localhost", 25042);
		}

		private void Update()
		{
			if (_manager.IsRunning)
			{
				_manager.PollEvents();
			}
		}

		private void OnDisable()
		{
			_manager.Stop();
		}
	}
}
