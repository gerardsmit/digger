﻿using Digger.Common.Game.Block;
using Digger.Common.Network.Packet;
using Digger.Common.Network.Packet.Server;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;

namespace Digger.Client.Network
{
    public class NetEventListener : INetEventListener
    {
        private readonly NetworkBehavior _networkBehavior;

        public NetEventListener(NetworkBehavior networkBehavior)
        {
            _networkBehavior = networkBehavior;
        }

        public void OnPeerConnected(NetPeer peer)
        {
            Debug.Log("Connected to the server.");
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            
        }

        public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
        {
            
        }

        public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
        {
            var rawPacket = PacketFactory.CreatePacket(reader.GetUShort());
            rawPacket.Read(reader);

            if (rawPacket is PacketBlockIds)
            {
                var packet = (PacketBlockIds) rawPacket;

                Debug.Log("Received server block names.");
                var blockNames = packet.BlockNames;

                foreach (var blockName in blockNames)
                {
                    if (!string.IsNullOrEmpty(blockName) && !BlockRegistery.Has(blockName))
                    {
                        Debug.LogError($"The block {blockName} is not on the client!");
                    }
                }

                BlockRegistery.SetServerMappings(blockNames);
            }
            else if (rawPacket is PacketCreateChunk)
            {
                var packet = (PacketCreateChunk) rawPacket;
                var worldController = _networkBehavior.WorldController;
                var chunkController = worldController.LoadChunkController(packet.ChunkX, packet.ChunkZ);

                chunkController.Chunk.Blocks = packet.Blocks;
                chunkController.Dirty = true;

                foreach (var neighbour in worldController.GetChunkControllersAround(packet.ChunkX, packet.ChunkZ))
                {
                    neighbour.Dirty = true;
                }

                Debug.Log($"Received server chunk {packet.ChunkX}, {packet.ChunkZ}.");
            }
            else if (rawPacket is PacketUpdateBlock)
            {
                var packet = (PacketUpdateBlock) rawPacket;
                var worldController = _networkBehavior.WorldController;

                worldController.World.SetBlock(packet.X, packet.Y, packet.Z, BlockRegistery.Get(packet.BlockId));
            }
        }

        public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            
        }
    }
}