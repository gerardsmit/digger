﻿using Digger.Common.Game.Block;
using Digger.Common.Game.Map;
using Digger.Common.Utility;
using MeshData = Digger.Client.Behavior.Resource.MeshData;

namespace Digger.Client.Game.Block.Model
{
    public class BlockModelDefault : IBlockModel
    {
        /// <inheritdoc cref="IBlockModel.Build"/>
        public void Build(Chunk chunk, BlockBase block, MeshData meshData, int x, int y, int z)
        {
            if (ShouldMesh(chunk, Face.Top, x, y, z))
            {
                meshData.Quad(Face.Top, x, y, z, block.GetTexture(Face.Top), block.GetColor(Face.Top));
            }
    
            if (ShouldMesh(chunk, Face.Bot, x, y, z))
            {
                meshData.Quad(Face.Bot, x, y, z, block.GetTexture(Face.Bot), block.GetColor(Face.Bot));
            }
    
            if (ShouldMesh(chunk, Face.North, x, y, z))
            {
                meshData.Quad(Face.North, x, y, z, block.GetTexture(Face.North), block.GetColor(Face.North));
            }
    
            if (ShouldMesh(chunk, Face.East, x, y, z))
            {
                meshData.Quad(Face.East, x, y, z, block.GetTexture(Face.East), block.GetColor(Face.East));
            }
    
            if (ShouldMesh(chunk, Face.South, x, y, z))
            {
                meshData.Quad(Face.South, x, y, z, block.GetTexture(Face.South), block.GetColor(Face.South));
            }
    
            if (ShouldMesh(chunk, Face.West, x, y, z))
            {
                meshData.Quad(Face.West, x, y, z, block.GetTexture(Face.West), block.GetColor(Face.West));
            }
        }

        /// <summary>
        /// Check if the given side should generate a mesh.
        /// </summary>
        /// <param name="chunk">The chunk.</param>
        /// <param name="face">The face of the mesh.</param>
        /// <param name="x">The X-coord.</param>
        /// <param name="y">The Y-coord.</param>
        /// <param name="z">The Z-coord.</param>
        /// <returns>True if the mesh should be generated.</returns>
        public static bool ShouldMesh(Chunk chunk, Face face, int x, int y, int z)
        {
            // Move the coord.
            switch (face)
            {
                case Face.Top:
                    y++;
                    break;
                case Face.Bot:
                    y--;
                    break;
                case Face.North:
                    z++;
                    break;
                case Face.South:
                    z--;
                    break;
                case Face.East:
                    x++;
                    break;
                case Face.West:
                    x--;
                    break;
                default:
                    throw new System.Exception("Unexpected side.");
            }

            // Under the chunk. Most of the times the player can not reach this; do not render.
            if (y < 0)
            {
                return false;
            }

            // Above the chunk height. The player can still reach this; render.
            if (y >= Chunk.ChunkHeight)
            {
                return true;
            }

            var block = chunk.GetBlock(x, y, z);

            // The chunk is not loaded, do not render.
            if (block == null)
            {
                return false;
            }

            return !block.IsSolid(face.Opposite());
        }
    }
}