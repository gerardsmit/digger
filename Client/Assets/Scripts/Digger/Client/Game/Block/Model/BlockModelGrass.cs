﻿using Digger.Common.Game.Block;
using Digger.Common.Game.Map;
using Digger.Common.Utility;
using MeshData = Digger.Client.Behavior.Resource.MeshData;

namespace Digger.Client.Game.Block.Model
{
    public class BlockModelGrass : IBlockModel
    {
        /// <inheritdoc cref="IBlockModel.Build"/>
        public void Build(Chunk chunk, BlockBase block, MeshData meshData, int x, int y, int z)
        {
            var sideTexture = block.GetTexture("side");
            var sideColor = block.GetColor("side");
            
            if (BlockModelDefault.ShouldMesh(chunk, Face.Top, x, y, z))
            {
                meshData.Quad(Face.Top, x, y, z, block.GetTexture(Face.Top), block.GetColor(Face.Top));
            }
            
            if (BlockModelDefault.ShouldMesh(chunk, Face.Bot, x, y, z))
            {
                meshData.Quad(Face.Bot, x, y, z, block.GetTexture(Face.Bot), block.GetColor(Face.Bot));
            }
            
            if (BlockModelDefault.ShouldMesh(chunk, Face.North, x, y, z))
            {
                meshData.Quad(Face.North, x, y, z, block.GetTexture(Face.North), block.GetColor(Face.North));
                meshData.Quad(Face.North, x, y, z, sideTexture, sideColor);
            }
            
            if (BlockModelDefault.ShouldMesh(chunk, Face.East, x, y, z))
            {
                meshData.Quad(Face.East, x, y, z, block.GetTexture(Face.East), block.GetColor(Face.East));
                meshData.Quad(Face.East, x, y, z, sideTexture, sideColor);
            }
            
            if (BlockModelDefault.ShouldMesh(chunk, Face.South, x, y, z))
            {
                meshData.Quad(Face.South, x, y, z, block.GetTexture(Face.South), block.GetColor(Face.South));
                meshData.Quad(Face.South, x, y, z, sideTexture, sideColor);
            }
            
            if (BlockModelDefault.ShouldMesh(chunk, Face.West, x, y, z))
            {
                meshData.Quad(Face.West, x, y, z, block.GetTexture(Face.West), block.GetColor(Face.West));
                meshData.Quad(Face.West, x, y, z, sideTexture, sideColor);
            }
        }
    }
}