﻿using Digger.Client.Behavior.Resource;
using Digger.Common.Game.Block;
using Digger.Common.Game.Map;

namespace Digger.Client.Game.Block.Model
{
    public interface IBlockModel
    {
        /// <summary>
        /// Build the block model.
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="block"></param>
        /// <param name="meshData"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        void Build(Chunk chunk, BlockBase block, MeshData meshData, int x, int y, int z);
    }
}