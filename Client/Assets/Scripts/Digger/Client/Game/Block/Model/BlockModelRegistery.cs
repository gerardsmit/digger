﻿using System;
using System.Collections.Generic;

namespace Digger.Client.Game.Block.Model
{
    public static class BlockModelRegistery
    {
        private static readonly Dictionary<string, IBlockModel> BlockModels = new Dictionary<string, IBlockModel>
        {
            {"default", new BlockModelDefault()},
            {"none", new BlockModelNone()},
            {"grass", new BlockModelGrass()}
        };

        public static IBlockModel Get(string name)
        {
            if (!BlockModels.ContainsKey(name))
            {
                throw new Exception($"Could not find the block model {name}.");
            }
            
            return BlockModels[name];
        }
    }
}