﻿using Digger.Client.Behavior.Resource;
using Digger.Common.Game.Block;
using Digger.Common.Game.Map;

namespace Digger.Client.Game.Block.Model
{
    public class BlockModelNone : IBlockModel
    {
        /// <inheritdoc cref="IBlockModel.Build"/>
        public void Build(Chunk chunk, BlockBase block, MeshData meshData, int x, int y, int z)
        {
        }
    }
}