﻿using UnityEngine;

namespace Digger.Client.Behavior.Camera
{
    public class CameraController : MonoBehaviour
    {
        public float Sensitivity = 10f;
        public GameObject Target;

        public Vector3 Offset = new Vector3(0, 0, 0);
        private float _rotationX;

        private void LateUpdate()   
        {
            if (Target == null) return;
            
            transform.position = Target.transform.position + Offset;

            _rotationX -= Input.GetAxis("Mouse Y") * Sensitivity;
            _rotationX = Mathf.Clamp(_rotationX, -90f, 90f);

            transform.localEulerAngles = new Vector3(_rotationX, Target.transform.localEulerAngles.y, 0);
        }
    }
}
