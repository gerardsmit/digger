﻿using Digger.Client.Behavior.Camera;
using UnityEngine;

namespace Digger.Client.Behavior.Player
{
    public class PlayerController : MonoBehaviour
    {
        public float Speed = 6.0F;
        public float JumpSpeed = 8.0F;
        public float Gravity = 20.0F;
        public float Sensitivity = 10f;
        
        private Vector3 _moveDirection = Vector3.zero;

        private void Start()
        {
            var followScript = UnityEngine.Camera.main.GetComponent<CameraController>();
            followScript.Target = gameObject;
        }

        private void Update()
        {
            var controller = GetComponent<CharacterController>();
            if (controller.isGrounded)
            {
                _moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                _moveDirection = transform.TransformDirection(_moveDirection);
                _moveDirection *= Speed;

                if (Input.GetButton("Jump"))
                    _moveDirection.y = JumpSpeed;
            }

            transform.Rotate(0, Input.GetAxis("Mouse X") * Sensitivity, 0);
            _moveDirection.y -= Gravity * Time.deltaTime;
            controller.Move(_moveDirection * Time.deltaTime);
        }
    }
}
