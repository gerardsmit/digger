﻿using Digger.Client.Game.Block.Model;
using Digger.Common.Game.Map;
using UnityEngine;
using MeshData = Digger.Client.Behavior.Resource.MeshData;

namespace Digger.Client.Behavior.Map
{
    public class ChunkMeshBuilder
    {
        public bool DidUpdate { get; private set; }
        
        private readonly Chunk _chunk;
        private readonly MeshData _meshData;

        public ChunkMeshBuilder(Chunk chunk, MeshData meshData)
        {
            _chunk = chunk;
            _meshData = meshData;
        }

        /// <summary>
        /// Build the mesh data for the chunk.
        /// </summary>
        public void Build()
        {
            lock (_meshData)
            {
                _meshData.Clear();
            
                for (var x = 0; x < Chunk.ChunkSize; x++)
                {
                    for (var z = 0; z < Chunk.ChunkSize; z++)
                    {
                        for (var y = 0; y < Chunk.ChunkHeight; y++)
                        {
                            var block = _chunk.GetBlock(x, y, z);
                            var model = BlockModelRegistery.Get(block.Model);
                            
                            model.Build(_chunk, block, _meshData, x, y, z);
                        }
                    }
                }
            }

            DidUpdate = true;
        }

        /// <summary>
        /// Update the mesh from the mesh data.
        /// </summary>
        /// <param name="mesh">The mesh to update.</param>
        /// <param name="meshCollider">The mesh collider to update.</param>
        public void UpdateMesh(Mesh mesh, MeshCollider meshCollider)
        {
            lock (_meshData)
            {
                DidUpdate = false;
                _meshData.Update(mesh, meshCollider);
            }
        }
    }
}