﻿using Digger.Common.Game.Map;

namespace Digger.Client.Behavior.Map
{
    public class ClientChunkProvider : IChunkProvider
    {
        public World World { get; set; }

        private readonly WorldController _worldController;

        public ClientChunkProvider(WorldController worldController)
        {
            _worldController = worldController;
        }

        public Chunk GetLoadedChunk(int x, int z)
        {
            var chunkController = _worldController.GetChunkController(x, z);
            return chunkController == null ? null : chunkController.Chunk;
        }

        public bool LoadChunk(int x, int z)
        {
            return false;
        }

        public void UnloadChunks()
        {
            
        }
    }
}