﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Digger.Client.Behavior.Resource;
using Digger.Common.Game.Map;
using Digger.Common.Utility;
using UnityEngine;

namespace Digger.Client.Behavior.Map
{
    public class WorldController : MonoBehaviour
    {
        /// <summary>
        /// The prefab of the chunk.
        /// 
        /// Whenever a new chunk is created this prefab will be cloned.
        /// </summary>
        public GameObject ChunkPrefab;

        /// <summary>
        /// The texture builder.
        /// </summary>
        public TextureBuilder TextureBuilder { get; private set; }
        
        /// <summary>
        /// The world.
        /// </summary>
        public World World { get; }

        /// <summary>
        /// The loaded chunk controllers.
        /// </summary>
        private readonly ConcurrentDictionary<Pair<int, int>, ChunkController> _chunksControllers;
        
        /// <summary>
        /// The chunk mesh watcher.
        /// </summary>
        private readonly ChunkMeshWatcher _chunkMeshWatcher;

        /// <summary>
        /// The loaded chunk controllers.
        /// </summary>
        public IEnumerable<ChunkController> ChunkControllers => _chunksControllers.Values;

        /// <summary>
        /// WorldController constructor.
        /// </summary>
        public WorldController()
        {
            World = new World(new ClientChunkProvider(this));
            
            _chunksControllers = new ConcurrentDictionary<Pair<int, int>, ChunkController>();
            _chunkMeshWatcher = new ChunkMeshWatcher(this);
        }

        /// <summary>
        /// Get the chunk at the given chunk coords.
        /// </summary>
        /// <param name="x">The X coord.</param>
        /// <param name="z">The Z coord.</param>
        /// <returns>The chunk.</returns>
        public ChunkController GetChunkController(int x, int z)
        {
            var key = Pair.Create(x, z);
            return _chunksControllers.ContainsKey(key) ? _chunksControllers[key] : null;
        }

        /// <summary>
        /// Get the chunks around the given chunk coords.
        /// </summary>
        /// <param name="x">The X coord.</param>
        /// <param name="z">The Z coord.</param>
        /// <returns>The chunk.</returns>
        public IEnumerable<ChunkController> GetChunkControllersAround(int x, int z)
        {
            var controllers = new List<ChunkController>();
            
            for (var cx = x - 1; cx < x + 1; cx++)
            {
                for (var cz = z - 1; cz< z + 1; cz++)
                {
                    if (cx == x && cz == z)
                    {
                        continue;
                    }
                    
                    var controller = GetChunkController(cx, cz);
                    if (controller != null)
                    {
                        controllers.Add(controller);
                    }
                }
            }

            return controllers;
        }

        /// <summary>
        /// Load the chunk for the given X and Z-coord.
        /// </summary>
        /// <param name="x">The x coord.</param>
        /// <param name="z">The z coord.</param>
        public ChunkController LoadChunkController(int x, int z)
        {
            // Check if the chunk is loaded.
            var chunk = GetChunkController(x, z);
            if (chunk != null)
            {
                return chunk;
            }

            // Create the chunk.
            var chunkObject = Instantiate(ChunkPrefab, new Vector3(x * Chunk.ChunkSize, 0, z * Chunk.ChunkSize), new Quaternion(), transform);

            chunkObject.name = $"Chunk {x}, {z}";
            chunkObject.GetComponent<MeshRenderer>().material = TextureBuilder.Material;

            chunk = chunkObject.GetComponent<ChunkController>();
            chunk.Initialize(this, x, z);

            // Register chunk.
            if (!_chunksControllers.TryAdd(Pair.Create(x, z), chunk))
            {
                Debug.LogWarning("Could not add the chunk.");
            }
            return chunk;
        }

        private void Awake()
        {
            TextureBuilder = GetComponent<TextureBuilder>();
        }
        
        private void Start()
        {
            _chunkMeshWatcher.Start();
        }

        private void OnDisable()
        {
            _chunkMeshWatcher.Stop();
        }
    }
}
