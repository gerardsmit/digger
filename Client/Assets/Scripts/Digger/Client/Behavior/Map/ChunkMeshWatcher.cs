﻿using System;
using System.Threading;
using UnityEngine;

namespace Digger.Client.Behavior.Map
{
    public class ChunkMeshWatcher
    {
        /// <summary>
        /// The World Controller.
        /// </summary>
        private readonly WorldController _worldController;
        
        /// <summary>
        /// The thread of the watcher.
        /// </summary>
        private readonly Thread _thread;
        
        /// <summary>
        /// True if the watcher is enabled.
        /// </summary>
        private bool _active;

        /// <summary>
        /// ChunkMeshWatcher constructor.
        /// </summary>
        /// <param name="worldController">The world controller.</param>
        public ChunkMeshWatcher(WorldController worldController)
        {
            _worldController = worldController;
            _thread = new Thread(Update) {Name = "Chunk Mesh Watcher"};
        }

        /// <summary>
        /// Start the watcher.
        /// </summary>
        public void Start()
        {
            if (_active)
            {
                return;
            }
            
            _active = true;
            _thread.Start();
        }

        /// <summary>
        /// Stop the watcher.
        /// </summary>
        public void Stop()
        {
            _active = false;
        }

        /// <summary>
        /// The thread method.
        /// </summary>
        private void Update()
        {
            while (_active)
            {
                try
                {
                    foreach (var chunkController in _worldController.ChunkControllers)
                    {
                        if (!chunkController.Dirty)
                        {
                            continue;
                        }
                            
                        chunkController.Dirty = false;
                        Debug.Log($"Rebuilding mesh for the chunk {chunkController.Chunk.ChunkX}, {chunkController.Chunk.ChunkZ}.");
                        chunkController.MeshBuilder.Build();
                    }
                        
                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    // TODO: Better error handling
                    Debug.LogError(ex.Message + ": " + ex.StackTrace);
                }
            }
        }
    }
}