﻿using Digger.Client.Behavior.Resource;
using Digger.Common.Game.Block;
using Digger.Common.Game.Map;
using UnityEngine;

namespace Digger.Client.Behavior.Map
{
    public class ChunkController : MonoBehaviour
    {
        public Chunk Chunk { get; private set; }
        public ChunkMeshBuilder MeshBuilder { get; private set; }

        public bool Dirty;

        private Mesh _mesh;
        private MeshCollider _meshCollider;

        /// <summary>
        /// Initialize the chunk.
        /// </summary>
        /// <param name="worldController"></param>
        /// <param name="x"></param>
        /// <param name="z"></param>
        public void Initialize(WorldController worldController, int x, int z)
        {
            Chunk = new Chunk(worldController.World, x, z);
            MeshBuilder = new ChunkMeshBuilder(Chunk, new MeshData(worldController.TextureBuilder));
            _mesh = GetComponent<MeshFilter>().mesh;
            _meshCollider = GetComponent<MeshCollider>();
        }

        /// <summary>
        /// Get the block in the chunk.
        /// </summary>
        /// <param name="x">The X coord.</param>
        /// <param name="y">The Y coord.</param>
        /// <param name="z">The Z coord.</param>
        /// <returns>The block.</returns>
        public BlockBase GetBlock(int x, int y, int z)
        {
            return Chunk.GetBlock(x, y, z);
        }

        private void Update()
        {
            if (MeshBuilder.DidUpdate)
            {
                Debug.Log("Updating mesh in the main thread.");
                MeshBuilder.UpdateMesh(_mesh, _meshCollider);
            }
        }
    }
}
