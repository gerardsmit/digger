﻿using System;
using System.Collections.Concurrent;
using System.IO;
using UnityEngine;

namespace Digger.Client.Behavior.Resource
{
    public class TextureBuilder : MonoBehaviour
    {
        /// <summary>
        /// The shader of the texture.
        /// </summary>
        public Shader Shader;

        /// <summary>
        /// The output material.
        /// </summary>
        public Material Material { get; private set; }

        /// <summary>
        /// The offsets of the blocks.
        /// </summary>
        private ConcurrentDictionary<string, Vector2> _offsets;

        /// <summary>
        /// The offset of the texture.
        /// </summary>
        private int _offset;

        /// <summary>
        /// True if the material is initialized.
        /// </summary>
        private bool _isInitialized;

        /// <summary>
        /// The texture.
        /// </summary>
        private Texture2D _texture;

        /// <summary>
        /// Get the offset for the given id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The offset.</returns>
        public Vector2 GetOffset(string id)
        {
            Vector2 offset;
            
            if (!_offsets.TryGetValue(id, out offset))
            {
                Debug.LogWarningFormat("The texture {0} is not found.", id);
            }

            return offset;
        }

        /// <summary>
        /// Load the given file.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="filePath">The file path.</param>
        public void Load(string id, string filePath)
        {
            var temp = new Texture2D(16, 16, TextureFormat.ARGB32, false);
            var x = _offset % 16;
            var y = (int) Math.Floor(_offset / (double) 16);
            
            if (_offsets.TryAdd(id, new Vector2(x, y)))
            {
                _offset++;
                
                temp.LoadImage(File.ReadAllBytes(filePath));
                _texture.SetPixels32(x * 16, y * 16, 16, 16, temp.GetPixels32());
                _texture.Apply();
            }
            else
            {
                Debug.LogWarningFormat("Failed to register the texture {0}.", id);
            }
        }

        private void Awake()
        {
            Debug.Log("Creating block texture.");
            
            _offsets = new ConcurrentDictionary<string, Vector2>();
            _texture = new Texture2D(256, 256, TextureFormat.ARGB32, false)
            {
                wrapMode = TextureWrapMode.Clamp,
                filterMode = FilterMode.Point
            };
            
            // Initialize the default textures.
            var path = Path.Combine(Path.Combine(Application.streamingAssetsPath, "Texture"), "Block");
            foreach (var filePath in Directory.GetFiles(path))
            {
                if (filePath.EndsWith(".meta"))
                {
                    continue;
                }

                var id = Path.GetFileNameWithoutExtension(filePath);
                Load(id, filePath);
            }

            Material = new Material(Shader);
            Material.SetTexture("_MainTex", _texture);
        }
    }
}
