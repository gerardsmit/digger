﻿using System.Collections.Generic;
using Digger.Common.Utility;
using MoonSharp.Interpreter;
using UnityEngine;

namespace Digger.Client.Behavior.Resource
{
    [MoonSharpUserData]
    public class MeshData
    {
        private const float Unit = 16f / 256f;

        private readonly List<Vector3> _vertices = new List<Vector3>();
        private readonly List<Color32> _colors = new List<Color32>();
        private readonly List<int> _triangles = new List<int>();
        private readonly List<Vector2> _uv = new List<Vector2>();
        private readonly TextureBuilder _textureBuilder;

        public MeshData(TextureBuilder textureBuilder)
        {
            _textureBuilder = textureBuilder;
        }

        public void Quad(Face side, int x, int y, int z, string texture)
        {
            Quad(side, x, y, z, texture, UnityEngine.Color.white);
        }

        public void Quad(Face side, float x, float y, float z, string texture, Common.Utility.Color color)
        {
            Quad(side, x, y, z, texture, new Color32(color.R, color.G, color.B, color.A));
        }

        public void Quad(Face side, float x, float y, float z, string texture, Color32 color)
        {
            var texturePos = _textureBuilder.GetOffset(texture);

            _triangles.Add(_vertices.Count); //1
            _triangles.Add(_vertices.Count + 1); //2
            _triangles.Add(_vertices.Count + 2); //3
            _triangles.Add(_vertices.Count); //1
            _triangles.Add(_vertices.Count + 2); //3
            _triangles.Add(_vertices.Count + 3); //4

            switch (side)
            {
                case Face.Top:
                    _vertices.Add(new Vector3(x, y, z + 1));
                    _vertices.Add(new Vector3(x + 1, y, z + 1));
                    _vertices.Add(new Vector3(x + 1, y, z));
                    _vertices.Add(new Vector3(x, y, z));
                    break;
                case Face.Bot:
                    _vertices.Add(new Vector3(x, y - 1, z));
                    _vertices.Add(new Vector3(x + 1, y - 1, z));
                    _vertices.Add(new Vector3(x + 1, y - 1, z + 1));
                    _vertices.Add(new Vector3(x, y - 1, z + 1));
                    break;
                case Face.North:
                    _vertices.Add(new Vector3(x + 1, y - 1, z + 1));
                    _vertices.Add(new Vector3(x + 1, y, z + 1));
                    _vertices.Add(new Vector3(x, y, z + 1));
                    _vertices.Add(new Vector3(x, y - 1, z + 1));
                    break;
                case Face.East:
                    _vertices.Add(new Vector3(x + 1, y - 1, z));
                    _vertices.Add(new Vector3(x + 1, y, z));
                    _vertices.Add(new Vector3(x + 1, y, z + 1));
                    _vertices.Add(new Vector3(x + 1, y - 1, z + 1));
                    break;
                case Face.South:
                    _vertices.Add(new Vector3(x, y - 1, z));
                    _vertices.Add(new Vector3(x, y, z));
                    _vertices.Add(new Vector3(x + 1, y, z));
                    _vertices.Add(new Vector3(x + 1, y - 1, z));
                    break;
                case Face.West:
                    _vertices.Add(new Vector3(x, y - 1, z + 1));
                    _vertices.Add(new Vector3(x, y, z + 1));
                    _vertices.Add(new Vector3(x, y, z));
                    _vertices.Add(new Vector3(x, y - 1, z));
                    break;
                default:
                    throw new System.Exception("Unexpected face.");
            }

            _colors.Add(color);
            _colors.Add(color);
            _colors.Add(color);
            _colors.Add(color);

            _uv.Add(new Vector2(Unit * texturePos.x + Unit, Unit * texturePos.y));
            _uv.Add(new Vector2(Unit * texturePos.x + Unit, Unit * texturePos.y + Unit));
            _uv.Add(new Vector2(Unit * texturePos.x, Unit * texturePos.y + Unit));
            _uv.Add(new Vector2(Unit * texturePos.x, Unit * texturePos.y));
        }

        [MoonSharpHidden]
        public void Clear()
        {
            _vertices.Clear();
            _uv.Clear();
            _triangles.Clear();
            _colors.Clear();
        }

        [MoonSharpHidden]
        public void Update(Mesh mesh, MeshCollider col)
        {
            mesh.Clear();
            mesh.vertices = _vertices.ToArray();
            mesh.uv = _uv.ToArray();
            mesh.triangles = _triangles.ToArray();
            mesh.colors32 = _colors.ToArray();
            mesh.RecalculateNormals();

            col.sharedMesh = null;
            col.sharedMesh = mesh;
        }
    }
}
