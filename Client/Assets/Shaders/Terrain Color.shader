﻿ Shader "Terrain"
 {
     Properties
     {
         _Color ("Main Color", Color) = (1,1,1,1)
         _MainTex ("Texture", 2D) = "white" {}
     }
 
     SubShader
     {
         Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}

         LOD 300
		 Blend SrcAlpha OneMinusSrcAlpha
         
         CGPROGRAM
         #pragma surface surf Lambert alphatest:_Cutoff
 
         sampler2D _MainTex;
         sampler2D _AlphaTex;
         fixed4 _Color;
 
         struct Input
         {
             float2 uv_MainTex;
			 float4 color: Color;
         };
 
         void surf (Input IN, inout SurfaceOutput o)
         {
             fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
             o.Albedo = c.rgb * _LightColor0 * IN.color;
			 o.Alpha = c.a;
         }
         ENDCG
     }
 
     FallBack "Transparent/Cutout/Diffuse"
 }