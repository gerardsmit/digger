local r = 20
local i = 0
local active = false

hook.Add("Update", function(world)
    if input.IsKeyDown(KeyCode.Q) then
        active = not active
    end

    if active and i <= 360 then 
        local angle = i * math.pi / 180
        local x, z = r * math.cos(angle), r * math.sin(angle)
        world.SetBlock(x, 60, z, blockRegistery.Dirt)

        i = i + 1
    end
end)