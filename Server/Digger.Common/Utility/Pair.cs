﻿using System.Collections.Generic;

namespace Digger.Common.Utility
{
    public class Pair<T1, T2>
    {
        public readonly T1 First;
        public readonly T2 Second;
        
        internal Pair(T1 first, T2 second)
        {
            First = first;
            Second = second;
        }

        public override bool Equals(object obj)
        {
            return obj is Pair<T1, T2> pair &&
                   EqualityComparer<T1>.Default.Equals(First, pair.First) &&
                   EqualityComparer<T2>.Default.Equals(Second, pair.Second);
        }

        public override int GetHashCode()
        {
            var hashCode = 43270662;
            hashCode = hashCode * -1521134295 + EqualityComparer<T1>.Default.GetHashCode(First);
            hashCode = hashCode * -1521134295 + EqualityComparer<T2>.Default.GetHashCode(Second);
            return hashCode;
        }
    }

    public static class Pair
    {
        public static Pair<T1, T2> Create<T1, T2>(T1 first, T2 second)
        {
            return new Pair<T1, T2>(first, second);
        }
    }
}