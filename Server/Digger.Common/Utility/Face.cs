﻿using System;
using Digger.Common.Game.Block;

namespace Digger.Common.Utility
{
    public enum Face {
        North,
        East,
        South,
        West,
        Top,
        Bot
    }

    public static class FaceMethods
    {
        public static Face Opposite(this Face face)
        {
            switch (face)
            {
                case Face.Top:
                    return Face.Bot;
                case Face.Bot:
                    return Face.Top;
                case Face.North:
                    return Face.South;
                case Face.East:
                    return Face.West;
                case Face.South:
                    return Face.North;
                case Face.West:
                    return Face.East;
                default:
                    throw new Exception("Unexpected face.");
            }
        }
        
        public static string GetTexture(this BlockBase blockBase, Face face)
        {
            return blockBase.GetTexture(face.ToString().ToLower());
        }
        
        public static Color GetColor(this BlockBase blockBase, Face face)
        {
            return blockBase.GetColor(face.ToString().ToLower());
        }
    }
}