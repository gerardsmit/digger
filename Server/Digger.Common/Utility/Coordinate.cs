﻿namespace Digger.Common.Utility
{
    public static class Coordinate
    {
        public static int GetIndex(int x, int y, int z)
        {
            return y << 8 | z << 4 | x;
        }
    }
}