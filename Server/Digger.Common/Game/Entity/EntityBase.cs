﻿using System.Numerics;
using Digger.Common.Game.Map;

namespace Digger.Common.Game.Entity
{
    public class EntityBase
    {
        public Vector3 Position;
        public Vector3 Rotation;
        public World World;
    }
}