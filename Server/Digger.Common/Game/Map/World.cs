﻿using System;
using System.Collections.Concurrent;
using Digger.Common.Game.Block;
using Digger.Common.Game.Entity;

namespace Digger.Common.Game.Map
{
    public class World
    {
        /// <summary>
        /// The chunk provider.
        /// </summary>
        private readonly IChunkProvider _chunkProvider;

        /// <summary>
        /// The entities in the world.
        /// </summary>
        private readonly ConcurrentDictionary<ushort, EntityBase> _entities;

        public World(IChunkProvider chunkProvider)
        {
            _chunkProvider = chunkProvider;
            _entities = new ConcurrentDictionary<ushort, EntityBase>();
            
            _chunkProvider.World = this;
        }

        /// <summary>
        /// Get the block at the given coords.
        /// </summary>
        /// <param name="x">The X coord.</param>
        /// <param name="y">The Y coord.</param>
        /// <param name="z">The Z coord.</param>
        /// <returns>The block.</returns>
        public BlockBase GetBlock(int x, int y, int z)
        {
            var chunk = GetChunkByWorld(x, z);
            return chunk?.GetBlock(x - chunk.WorldX, y, z - chunk.WorldZ);
        }

        /// <summary>
        /// Set the block at the given coords.
        /// </summary>
        /// <param name="x">The X coord.</param>
        /// <param name="y">The Y coord.</param>
        /// <param name="z">The Z coord.</param>
        /// <param name="block">The block.</param>
        /// <returns>True on success.</returns>
        public bool SetBlock(int x, int y, int z, BlockBase block)
        {
            var chunk = GetChunkByWorld(x, z);
            if (chunk == null)
            {
                return false;
            }
            
            chunk.SetBlock(x - chunk.WorldX, y, z - chunk.WorldZ, block);
            return true;
        }
        
        /// <summary>
        /// Get the chunk at the given chunk coords.
        /// </summary>
        /// <param name="x">The X coord.</param>
        /// <param name="z">The Z coord.</param>
        /// <returns>The chunk.</returns>
        public Chunk GetChunk(int x, int z)
        {
            return _chunkProvider.GetLoadedChunk(x, z);
        }

        /// <summary>
        /// Get the chunk by world positions.
        /// </summary>
        /// <param name="worldX">The X coord.</param>
        /// <param name="worldZ">The Z coord.</param>
        /// <returns>The chunk.</returns>
        public Chunk GetChunkByWorld(int worldX, int worldZ)
        {
            var x = (int) Math.Floor(worldX / (float) Chunk.ChunkSize);
            var z = (int) Math.Floor(worldZ / (float) Chunk.ChunkSize);
            return GetChunk(x, z);
        }
    }
}