﻿using Digger.Common.Game.Block;
using Digger.Common.Utility;

namespace Digger.Common.Game.Map
{
    public class Chunk
    {
        public const int ChunkSize = 16;
        public const int ChunkHeight = 128;
        public const int ChunkBlockSize = ChunkSize * ChunkSize * ChunkHeight;
        
        public readonly World World;
        public readonly int ChunkX;
        public readonly int ChunkZ;
        public ushort[] Blocks;
        
        public int WorldX => ChunkX * ChunkSize;
        public int WorldZ => ChunkZ * ChunkSize;
        
        public Chunk(World world, int chunkX, int chunkZ)
        {
            World = world;
            ChunkX = chunkX;
            ChunkZ = chunkZ;
            Blocks = new ushort[ChunkBlockSize];
        } 
        
        /// <summary>
        /// Set the block at the given coordinates.
        /// </summary>
        /// <param name="x">The X-coord.</param>
        /// <param name="y">The Y-coord.</param>
        /// <param name="z">The Z-coord.</param>
        /// <param name="block">The new block.</param>
        public virtual void SetBlock(int x, int y, int z, BlockBase block)
        {
            if (x < 0 || x >= ChunkSize || z < 0 || z >= ChunkSize)
            {
                World.SetBlock(WorldX + x, y, WorldZ + z, block);
                return;
            }
            
            Blocks[Coordinate.GetIndex(x, y, z)] = block.Id;
        }

        /// <summary>
        /// Get the block at the given coordinates.
        /// </summary>
        /// <param name="x">The X-coord.</param>
        /// <param name="y">The Y-coord.</param>
        /// <param name="z">The Z-coord.</param>
        /// <returns>The block.</returns>
        public BlockBase GetBlock(int x, int y, int z)
        {
            if (x < 0 || x >= ChunkSize || z < 0 || z >= ChunkSize)
            {
                return World.GetBlock(WorldX + x, y, WorldZ + z);
            }
            
            return BlockRegistery.Get(Blocks[Coordinate.GetIndex(x, y, z)]);
        }
    }
}