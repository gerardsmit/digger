﻿namespace Digger.Common.Game.Map
{
    public interface IChunkProvider
    {
        World World { get; set; }
        
        /// <summary>
        /// Get the loaded chunk from the given coordinates.
        /// </summary>
        /// <param name="x">The X-coord.</param>
        /// <param name="z">The Y-coord.</param>
        /// <returns>The chunk.</returns>
        Chunk GetLoadedChunk(int x, int z);
        
        /// <summary>
        /// Load the chunk at the given coordinates.
        /// </summary>
        /// <param name="x">The X-coord.</param>
        /// <param name="z">The Y-coord.</param>
        /// <returns>True if the chunk was loaded.</returns>
        bool LoadChunk(int x, int z);

        /// <summary>
        /// Unload the chunks that are marked.
        /// </summary>
        void UnloadChunks();
    }
}