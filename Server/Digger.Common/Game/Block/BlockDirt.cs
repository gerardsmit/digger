﻿namespace Digger.Common.Game.Block
{
    public class BlockDirt : BlockBase
    {
        public BlockDirt() : base("dirt")
        {
            Texture = "dirt";
        }
    }
}