﻿using Digger.Common.Utility;

namespace Digger.Common.Game.Block
{
    public class BlockAir : BlockBase
    {
        public BlockAir() : base("air")
        {
            Model = "none";
        }

        public override bool IsSolid(Face face)
        {
            return false;
        }
    }
}