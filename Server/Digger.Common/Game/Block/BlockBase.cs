﻿using Digger.Common.Game.Map;
using Digger.Common.Utility;

namespace Digger.Common.Game.Block
{
    public class BlockBase
    {
        public ushort Id;
        public readonly string Name;
        public string Texture = "unknown";
        public string Model = "default";
        public Color Color = Color.White;

        public BlockBase(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Check if the given face is solid.
        /// </summary>
        /// <param name="face">The face.</param>
        /// <returns>True if the face is solid.</returns>
        public virtual bool IsSolid(Face face)
        {
            return true;
        }

        /// <summary>
        /// Get the texture of the given id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The texture.</returns>
        public virtual string GetTexture(string id)
        {
            return Texture;
        }

        /// <summary>
        /// Get the color of the given id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The color.</returns>
        public virtual Color GetColor(string id)
        {
            return Color;
        }
    }
}