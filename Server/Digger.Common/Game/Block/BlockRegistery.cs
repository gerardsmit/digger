﻿using System;
using System.Collections.Generic;

namespace Digger.Common.Game.Block
{
    public static class BlockRegistery
    {
        public static string[] Mappings => Environment.Side == Side.Client ? _serverMappings : LocalMappings;
        private static readonly string[] LocalMappings;
        private static string[] _serverMappings;
        
        private static ushort _nextId;
        private static readonly Dictionary<string, BlockBase> Blocks;

        static BlockRegistery()
        {
            LocalMappings = new string[4096];
            Blocks = new Dictionary<string, BlockBase>();
            
            Register(new BlockAir());
            Register(new BlockDirt());
            Register(new BlockGrass());
        }

        /// <summary>
        /// Register a new block.
        /// </summary>
        /// <param name="block">The block.</param>
        public static void Register(BlockBase block)
        {
            Blocks.Add(block.Name, block);
            
            var id = _nextId++;
            block.Id = id;
            LocalMappings[id] = block.Name;
        }

        /// <summary>
        /// Get the block by the ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The block.</returns>
        /// <exception cref="Exception">If the ID or block could not be found.</exception>
        public static BlockBase Get(ushort id)
        {
            var name = Mappings[id];

            if (name == null)
            {
                throw new Exception($"The block ID {id} could not be found.");
            }
            
            return Get(name);
        }

        /// <summary>
        /// Get the block by it's name.
        /// </summary>
        /// <param name="name">The name of the block.</param>
        /// <returns>The block.</returns>
        /// <exception cref="Exception">If the block could not be found.</exception>
        public static BlockBase Get(string name)
        {
            if (!Has(name))
            {
                throw new Exception($"The block {name} could not be found!");
            }
            
            return Blocks[name];
        }

        /// <summary>
        /// Checks if the block name is registered.
        /// </summary>
        /// <param name="name">The name of the block.</param>
        /// <returns>rue if the block is found.</returns>
        public static bool Has(string name)
        {
            return Blocks.ContainsKey(name);
        }

        /// <summary>
        /// Set the server block mappings.
        /// </summary>
        /// <param name="serverMappings">The server mappings.</param>
        /// <exception cref="Exception">If a block could not be found.</exception>
        public static void SetServerMappings(string[] serverMappings)
        {
            foreach (var blockName in serverMappings)
            {
                if (!string.IsNullOrEmpty(blockName) && !Has(blockName))
                {
                    throw new Exception($"Failed to set the server mappings. The block {blockName} could not be found.");
                }
            }
            
            _serverMappings = serverMappings;
        }
    }
}