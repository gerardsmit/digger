﻿using Digger.Common.Utility;

namespace Digger.Common.Game.Block
{
    public class BlockGrass : BlockBase
    {
        public Color GrassColor = Color.FromHex("018E0E");
        public string TextureTop = "grass";
        public string TextureSide = "grass_side";
        
        public BlockGrass() : base("grass")
        {
            Model = "grass";
            Texture = "dirt";
        }
    
        /// <inheritdoc cref="BlockBase.GetTexture"/>
        public override string GetTexture(string id)
        {
            switch (id)
            {
                case "side":
                    return TextureSide;
                case "top":
                    return TextureTop;
                default:
                    return base.GetTexture(id);
            }
        }
    
        /// <inheritdoc cref="BlockBase.GetColor"/>
        public override Color GetColor(string id)
        {
            switch (id)
            {
                case "top":
                case "side":
                    return GrassColor;
                default:
                    return base.GetColor(id);
            }
        }
    }
}