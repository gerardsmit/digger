﻿namespace Digger.Common
{
    public enum Side
    {
        Unknown,
        Client,
        Server
    }
}