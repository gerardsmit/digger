﻿using LiteNetLib;
using LiteNetLib.Utils;

namespace Digger.Common.Network.Packet
{
    public interface IPacket
    {
        SendOptions Options { get; }
        void Read(NetDataReader reader);
        void Write(NetDataWriter writer);
    }
}