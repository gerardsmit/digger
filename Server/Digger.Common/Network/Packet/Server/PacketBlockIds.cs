﻿using LiteNetLib;
using LiteNetLib.Utils;

namespace Digger.Common.Network.Packet.Server
{
    public class PacketBlockIds : IPacket
    {
        public SendOptions Options { get; } = SendOptions.ReliableUnordered;
        
        public string[] BlockNames;

        public PacketBlockIds()
        {
        }

        public PacketBlockIds(string[] blockNames)
        {
            BlockNames = blockNames;
        }

        public void Read(NetDataReader reader)
        {
            BlockNames = reader.GetStringArray(256);
        }

        public void Write(NetDataWriter writer)
        {
            writer.PutArray(BlockNames);
        }
    }
}