﻿using System;
using Digger.Common.Game.Map;
using LiteNetLib;
using LiteNetLib.Utils;

namespace Digger.Common.Network.Packet.Server
{
    public class PacketCreateChunk : IPacket
    {
        public SendOptions Options { get; } = SendOptions.ReliableUnordered;

        public int ChunkX;
        public int ChunkZ;
        public ushort[] Blocks;

        public PacketCreateChunk()
        {
        }

        public PacketCreateChunk(Chunk chunk)
        {
            ChunkX = chunk.ChunkX;
            ChunkZ = chunk.ChunkZ;
            Blocks = chunk.Blocks;
        }

        public void Read(NetDataReader reader)
        {
            ChunkX = reader.GetInt();
            ChunkZ = reader.GetInt();
            Blocks = reader.GetUShortArray();
        }

        public void Write(NetDataWriter writer)
        {
            if (Blocks.Length != Chunk.ChunkBlockSize)
            {
                throw new Exception("Invalid block size.");
            }
            
            writer.Put(ChunkX);
            writer.Put(ChunkZ);
            writer.PutArray(Blocks);
        }
    }
}