﻿using LiteNetLib;
using LiteNetLib.Utils;

namespace Digger.Common.Network.Packet.Server
{
    public class PacketUpdateBlock : IPacket
    {
        public SendOptions Options { get; } = SendOptions.ReliableUnordered;

        public int X;
        public int Y;
        public int Z;
        public ushort BlockId;

        public PacketUpdateBlock()
        {
        }

        public PacketUpdateBlock(int x, int y, int z, ushort blockId)
        {
            X = x;
            Y = y;
            Z = z;
            BlockId = blockId;
        }

        public void Read(NetDataReader reader)
        {
            X = reader.GetInt();
            Y = reader.GetInt();
            Z = reader.GetInt();
            BlockId = reader.GetByte();
        }

        public void Write(NetDataWriter writer)
        {
            writer.Put(X);
            writer.Put(Y);
            writer.Put(Z);
            writer.Put(BlockId);
        }
    }
}