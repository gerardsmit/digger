﻿using System;
using System.Collections.Generic;
using Digger.Common.Network.Packet.Client;
using Digger.Common.Network.Packet.Server;

namespace Digger.Common.Network.Packet
{
    public static class PacketFactory
    {
        private static readonly Dictionary<ushort, Type> Packets = new Dictionary<ushort, Type>
        {
            {PacketHeader.BlockIds, typeof(PacketBlockIds)},
            {PacketHeader.CreateChunk, typeof(PacketCreateChunk)},
            {PacketHeader.UpdateBlock, typeof(PacketUpdateBlock)},
            {PacketHeader.UpdatePlayerTransform, typeof(PacketUpdatePlayerTransform)}
        };

        public static void Register(ushort header, Type type)
        {
            if (!typeof(IPacket).IsAssignableFrom(type))
            {
                throw new Exception("Expected an IPacket.");
            }

            Packets.Add(header, type);
        }

        public static IPacket CreatePacket(ushort header)
        {
            if (!Packets.TryGetValue(header, out var type))
            {
                throw new Exception($"Failed to get the packet from the header {header}.");
            }

            return (IPacket) Activator.CreateInstance(type);
        }

        public static ushort GetHeader(IPacket packet)
        {
            return GetHeader(packet.GetType());
        }

        public static ushort GetHeader(Type type)
        {
            foreach (var kv in Packets)
            {
                if (kv.Value == type)
                {
                    return kv.Key;
                }
            }

            throw new Exception("Could not find the packet header.");
        }
    }
}