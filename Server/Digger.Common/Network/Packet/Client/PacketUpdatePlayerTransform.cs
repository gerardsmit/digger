﻿using System.Numerics;
using LiteNetLib;
using LiteNetLib.Utils;

namespace Digger.Common.Network.Packet.Client
{
    public class PacketUpdatePlayerTransform : IPacket
    {
        public SendOptions Options { get; } = SendOptions.Unreliable;

        public Vector3 Position;
        public Vector3 Rotation;

        public void Read(NetDataReader reader)
        {
            Position = new Vector3(reader.GetFloat(), reader.GetFloat(), reader.GetFloat());
            Rotation = new Vector3(reader.GetFloat(), reader.GetFloat(), reader.GetFloat());
        }

        public void Write(NetDataWriter writer)
        {
            writer.Put(Position.X);
            writer.Put(Position.Y);
            writer.Put(Position.Z);
            writer.Put(Rotation.X);
            writer.Put(Rotation.Y);
            writer.Put(Rotation.Z);
        }
    }
}