﻿namespace Digger.Common.Network.Packet
{
    public static class PacketHeader
    {
        // Client > Server
        public const ushort CreateChunk = 1;
        
        // Server > Client
        public const ushort UpdateBlock = 2;
        public const ushort BlockIds = 3;
        public const ushort UpdatePlayerTransform = 4;
    }
}