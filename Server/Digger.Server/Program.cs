﻿using System.Collections.Generic;
using Digger.Common;

namespace Digger.Server
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            Environment.Side = Side.Server;
            
            var server = new Network.GameServer();
            server.Start(25042);
        }
    }
}