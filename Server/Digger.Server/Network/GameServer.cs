﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Digger.Common.Game.Map;
using Digger.Common.Network.Packet;
using Digger.Server.Game.Map;
using LiteNetLib;
using LiteNetLib.Utils;
using NLog;

namespace Digger.Server.Network
{
    public class GameServer
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        public readonly ConnectionManager ConnectionManager;
        public readonly ServerChunkProvider ChunkProvider;
        public readonly World World;
        
        private readonly NetManager _manager;

        public GameServer()
        {
            ConnectionManager = new ConnectionManager();
            _manager = new NetManager(new NetEventListener(this), "Digger");
            
            ChunkProvider = new ServerChunkProvider(this);
            World = new World(ChunkProvider);
        }

        public void SendToAll(IPacket packet)
        {
            var writer = new NetDataWriter();
            writer.Put(PacketFactory.GetHeader(packet));
            packet.Write(writer);
            _manager.SendToAll(writer, packet.Options);
        }

        public void Start(int port)
        {
            ChunkProvider.LoadChunk(0, 0);
            ChunkProvider.LoadChunk(0, 1);
            ChunkProvider.LoadChunk(1, 0);
            ChunkProvider.LoadChunk(1, 1);
            
            Logger.Info($"Starting the server on port {port}.");
            _manager.Start(port);

            while (!Console.KeyAvailable)
            {
                _manager.PollEvents();
                Thread.Sleep(15);
            }
        }
    }
}