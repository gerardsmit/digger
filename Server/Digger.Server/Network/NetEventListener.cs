﻿using System;
using System.Collections.Concurrent;
using Digger.Common.Game.Block;
using Digger.Common.Game.Map;
using Digger.Common.Network.Packet;
using Digger.Common.Network.Packet.Client;
using Digger.Common.Network.Packet.Server;
using LiteNetLib;
using LiteNetLib.Utils;

namespace Digger.Server.Network
{
    public class NetEventListener : INetEventListener
    {
        private readonly GameServer _server;

        public NetEventListener(GameServer server)
        {
            _server = server;
        }

        public void OnPeerConnected(NetPeer peer)
        {
            var connection = _server.ConnectionManager.RegisterPeer(peer);

            if (connection == null)
            {
                if (peer.ConnectionState != ConnectionState.Disconnected)
                {
                    peer.NetManager.DisconnectPeer(peer);
                }

                return;
            }

            connection.Send(new PacketBlockIds(BlockRegistery.Mappings));

            foreach (var chunk in _server.ChunkProvider.LoadedChunks)
            {
                connection.Send(new PacketCreateChunk(chunk));
            }
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            _server.ConnectionManager.RemovePeer(peer);
        }

        public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
        {
            
        }

        public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
        {
            var packet = PacketFactory.CreatePacket(reader.GetUShort());
            packet.Read(reader);
        }

        public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            
        }
    }
}