﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using LiteNetLib;
using NLog;

namespace Digger.Server.Network
{
    public class ConnectionManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        private ushort _nextId;
        private readonly ConcurrentDictionary<ushort, Connection> _connections;

        public ICollection<Connection> Connections => _connections.Values;

        public ConnectionManager()
        {
            _connections = new ConcurrentDictionary<ushort, Connection>();
        }

        public Connection RegisterPeer(NetPeer peer)
        {
            if (peer.ConnectionState == ConnectionState.Disconnected)
            {
                Logger.Trace("Cannot register the connection because the connection has been disconnected.");
                return null;
            }
            
            
            if (peer.Tag != null && peer.Tag is Connection tagConnection)
            {
                Logger.Trace("Cannot register the connection because the tag is already set.");
                return tagConnection;
            }

            var id = _nextId++;
            var connection = new Connection(id, peer);

            if (!_connections.TryAdd(id, connection))
            {
                Logger.Warn($"Failed to register the connection ID {id}.");
                return null;
            }
            
            peer.Tag = connection;
            Logger.Info($"Connection {id} has connected to the server.");
            return connection;
        }

        public void RemovePeer(NetPeer peer)
        {
            if (peer.Tag == null || !(peer.Tag is Connection connection))
            {
                Logger.Trace("Cannot remove connection, invalid peer tag.");
                return;
            }

            if (_connections.TryRemove(connection.Id, out _))
            {
                Logger.Info($"Connection {connection.Id} has disconnected from the server.");
            }
            else
            {
                Logger.Warn($"Failed to remove the connection ID {connection.Id}.");
            }
        }
    }
}