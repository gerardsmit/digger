﻿using Digger.Common.Game.Entity;
using Digger.Common.Network.Packet;
using LiteNetLib;
using LiteNetLib.Utils;

namespace Digger.Server.Network
{
    public class Connection
    {
        public readonly ushort Id;
        public readonly NetPeer Peer;
        public EntityPlayer Player;

        public Connection(ushort id, NetPeer peer)
        {
            Id = id;
            Peer = peer;
        }

        /// <summary>
        /// Send the given packet to the player.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public void Send(IPacket packet)
        {
            var writer = new NetDataWriter();
            writer.Put(PacketFactory.GetHeader(packet));
            packet.Write(writer);
            Peer.Send(writer, packet.Options);
        }
    }
}