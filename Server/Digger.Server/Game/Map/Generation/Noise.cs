﻿using System;
using System.Numerics;
using Digger.Server.Utility;

namespace Digger.Server.Game.Map.Generation
{
    /// <summary>
    /// Simplex Noise
    /// 
    /// Source: https://gist.github.com/jstanden/1489447
    /// </summary>
    public class Noise
    {
        private const float Onethird = 0.333333333f;
        private const float Onesixth = 0.166666667f;
        
        private readonly int[] _a = new int[3];
        private float _s, _u, _v, _w;
        private int _i, _j, _k;
        private readonly int[] _seed;

        public Noise()
        {
            var rand = new System.Random();
            _seed = new int[8];
            for (var q = 0; q < 8; q++)
                _seed[q] = rand.Next();
        }

        public Noise(string seed)
        {
            _seed = new int[8];
            var seedParts = seed.Split(' ');

            for (var q = 0; q < 8; q++)
            {
                int b;
                try
                {
                    b = int.Parse(seedParts[q]);
                }
                catch
                {
                    b = 0x0;
                }
                _seed[q] = b;
            }
        }

        public Noise(int[] seed)
        { // {0x16, 0x38, 0x32, 0x2c, 0x0d, 0x13, 0x07, 0x2a}
            _seed = seed;
        }

        public string GetSeed()
        {
            var seed = "";

            for (var q = 0; q < 8; q++)
            {
                seed += _seed[q].ToString();
                if (q < 7)
                    seed += " ";
            }

            return seed;
        }

        public int GetHeight(int x, int y, int z, float scale, int max)
        {
            return (int) Math.Floor((GenerateCoherent(x * scale, y * scale, z * scale) + 1f) * (max / 2f));
        }

        public float GenerateCoherent(float x, float y, float z, int octaves = 1, int multiplier = 25, float amplitude = 0.5f, float lacunarity = 2, float persistence = 0.9f)
        {
            var v3 = new Vector3(x, y, z) / multiplier;
            float val = 0;
            for (var n = 0; n < octaves; n++)
            {
                val += Generate(v3.X, v3.Y, v3.Z) * amplitude;
                v3 *= lacunarity;
                amplitude *= persistence;
            }
            return val;
        }

        public int GetDensity(Vector3 loc)
        {
            var val = GenerateCoherent(loc.X, loc.Y, loc.Z);
            return (int)MathHelper.Lerp(0, 255, val);
        }

        // Simplex Noise Generator
        public float Generate(float x, float y, float z)
        {
            _s = (x + y + z) * Onethird;
            _i = Fastfloor(x + _s);
            _j = Fastfloor(y + _s);
            _k = Fastfloor(z + _s);

            _s = (_i + _j + _k) * Onesixth;
            _u = x - _i + _s;
            _v = y - _j + _s;
            _w = z - _k + _s;

            _a[0] = 0; _a[1] = 0; _a[2] = 0;

            var hi = _u >= _w ? _u >= _v ? 0 : 1 : _v >= _w ? 1 : 2;
            var lo = _u < _w ? _u < _v ? 0 : 1 : _v < _w ? 1 : 2;

            return Kay(hi) + Kay(3 - hi - lo) + Kay(lo) + Kay(0);
        }

        private float Kay(int a)
        {
            _s = (_a[0] + _a[1] + _a[2]) * Onesixth;
            var x = _u - _a[0] + _s;
            var y = _v - _a[1] + _s;
            var z = _w - _a[2] + _s;
            var t = 0.6f - x * x - y * y - z * z;
            var h = Shuffle(_i + _a[0], _j + _a[1], _k + _a[2]);
            _a[a]++;
            if (t < 0) return 0;
            var b5 = h >> 5 & 1;
            var b4 = h >> 4 & 1;
            var b3 = h >> 3 & 1;
            var b2 = h >> 2 & 1;
            var b1 = h & 3;

            var p = b1 == 1 ? x : b1 == 2 ? y : z;
            var q = b1 == 1 ? y : b1 == 2 ? z : x;
            var r = b1 == 1 ? z : b1 == 2 ? x : y;

            p = b5 == b3 ? -p : p;
            q = b5 == b4 ? -q : q;
            r = b5 != (b4 ^ b3) ? -r : r;
            t *= t;
            return 8 * t * t * (p + (b1 == 0 ? q + r : b2 == 0 ? q : r));
        }

        private int Shuffle(int i, int j, int k)
        {
            return B(i, j, k, 0) + B(j, k, i, 1) + B(k, i, j, 2) + B(i, j, k, 3) + B(j, k, i, 4) + B(k, i, j, 5) + B(i, j, k, 6) + B(j, k, i, 7);
        }

        private int B(int i, int j, int k, int b)
        {
            return _seed[B(i, b) << 2 | B(j, b) << 1 | B(k, b)];
        }

        private static int B(int n, int b)
        {
            return n >> b & 1;
        }

        private static int Fastfloor(float n)
        {
            return n > 0 ? (int)n : (int)n - 1;
        }
    }
}