﻿using Digger.Common.Game.Block;
using Digger.Common.Game.Map;
using Digger.Common.Network.Packet.Server;
using Digger.Server.Network;

namespace Digger.Server.Game.Map
{
    public class ServerChunk : Chunk
    {
        public GameServer Server { get; set; }
        
        public ServerChunk(World world, int chunkX, int chunkZ) : base(world, chunkX, chunkZ)
        {
        }

        /// <inheritdoc cref="Chunk.SetBlock"/>
        public override void SetBlock(int x, int y, int z, BlockBase block)
        {
            base.SetBlock(x, y, z, block);
            
            Server.SendToAll(new PacketUpdateBlock(x, y, z, block.Id));
        }
    }
}