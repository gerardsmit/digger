﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Digger.Common.Game.Block;
using Digger.Common.Game.Map;
using Digger.Common.Utility;
using Digger.Server.Game.Map.Generation;
using Digger.Server.Network;
using NLog;

namespace Digger.Server.Game.Map
{
    public class ServerChunkProvider : IChunkProvider
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The noise used when generating chunks.
        /// </summary>
        private readonly Noise _noise;
        
        /// <summary>
        /// The world of the chunk provider.
        /// </summary>
        public World World { get; set; }

        /// <summary>
        /// The server.
        /// </summary>
        private GameServer _server;

        /// <summary>
        /// The loaded chunks.
        /// </summary>
        private readonly ConcurrentDictionary<Pair<int, int>, Chunk> _chunks;

        /// <summary>
        /// Get all the loaded chunks.
        /// </summary>
        public ICollection<Chunk> LoadedChunks => _chunks.Values;

        public ServerChunkProvider(GameServer server)
        {
            _server = server;
            _noise = new Noise();
            _chunks = new ConcurrentDictionary<Pair<int, int>, Chunk>();
        }

        /// <inheritdoc cref="IChunkProvider.GetLoadedChunk"/>
        public Chunk GetLoadedChunk(int x, int z)
        {
            return _chunks[Pair.Create(x, z)];
        }

        /// <inheritdoc cref="IChunkProvider.LoadChunk"/>
        public bool LoadChunk(int x, int z)
        {
            var key = Pair.Create(x, z);
            if (_chunks.ContainsKey(key))
            {
                return true;
            }
            
            var chunk = new ServerChunk(World, x, z);

            chunk.Server = _server;
            
            GenerateChunk(chunk);

            return _chunks.TryAdd(key, chunk);
        }

        /// <inheritdoc cref="IChunkProvider.UnloadChunks"/>
        public void UnloadChunks()
        {
            
        }

        /// <summary>
        /// Generate the given chunk.
        /// </summary>
        /// <param name="chunk">The chunk.</param>
        private void GenerateChunk(Chunk chunk)
        {
            Logger.Trace($"Generating chunk {chunk.ChunkX}, {chunk.ChunkZ}");

            var dirt = BlockRegistery.Get("grass");
            
            for (var x = 0; x < Chunk.ChunkSize; x++)
            {
                for (var z = 0; z < Chunk.ChunkSize; z++)
                {
                    var height = 20 + _noise.GetHeight(chunk.WorldX + x, 0, chunk.WorldZ + z, 1f, 48);
                    
                    for (var y = 0; y < height; y++)
                    {
                        chunk.SetBlock(x, y, z, dirt);
                    }
                }
            }
        }
    }
}